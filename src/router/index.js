import Vue from 'vue';
import Router from 'vue-router'

// Containers
const TheContainer = () =>
    import ('@/containers/TheContainer')

const Login = () =>
    import ('@/views/Login')

// Admin
const Dashboard = () =>
    import ('@/views/Dashboard')



const Users = () =>
    import ('@/views/admin/users/users')

const StockNorm = () =>
    import ('@/views/admin/stocknorm/stocknorms')

const StockTrend = () =>
    import ('@/views/admin/stocktrend/stocktrend')


const Departments = () =>
    import ('@/views/admin/departments/departments')

const Nodes = () =>
    import ('@/views/admin/node/nodes');

const Skumeta = () =>
    import ('@/views/admin/skumeta/skumetas');

const Location = () =>
     import('@/views/admin/location/locations');

const Skumaster = () =>
    import('@/views/admin/skumaster/skumasters');

const Nodesku = () =>
    import('@/views/admin/nodesku/nodeskus');

const Nodemap = () =>
    import('@/views/admin/nodemap/nodemaps');

const Nodemapchart = () =>
    import ('@/views/admin/nodemapchart/nodemapcharts');

const NodeSkuTopPerformer = () =>
    import ('@/views/admin/NodeSkuTopPerformer/NodeSkuTopPerformer');

const DispatchReport =() =>
    import ('@/views/admin/DispatchReport/DispatchReport');

const NodeStockNorm = () =>
import ('@/views/admin/nodeStockNorm/NodeStockNorm')

const ExpandedBarChart = () =>
import ('@/views/admin/dashboard/ExpandedBarChart')

const TrendReport = () =>
import ('@/views/admin/TrendReport/TrendReport')

const SupplierTrendReport = () =>
import ('@/views/admin/SupplierTrendReport/SupplierTrendReport')


const UserRole = () =>
import('@/views/admin/UserRole/userroles');


const SKUAttribute = () =>
import('@/views/admin/SKUAttribute/skuattributes');

// Use routes

const UserDashboard = () =>
import('@/views/user/Dashboard');

const UserStocknorm = () =>
import('@/views/user/stocknorm/Stocknorms');

const UserSupplierTrend = () =>
import('@/views/user/supplierTrend/SupplierTrend');

const UserSKUTrend = () =>
import('@/views/user/skuTrend/SkuTrend');


const UserConsumerTrend = () =>
import('@/views/user/consumerTrend/ConsumerTrend');

Vue.use(Router)

export default new Router({
    mode: 'hash', // https://router.vuejs.org/api/#mode
    linkActiveClass: 'active',
    scrollBehavior: () => ({ y: 0 }),
    routes: configRoutes()
})

function configRoutes() {
    return [{
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',

            component: Login,
        },
        {
            path: '/admin',
            redirect: '/login',
            component: TheContainer,
            children: [{
                    path: 'dashboard',
                    name: 'dashboard',
                    component: Dashboard
                },
                {
                    path: 'stocknorms',
                    name: 'Stock Norms',
                    component:StockNorm
                },
                {
                    path: 'stocktrendreport',
                    name: 'Stock Trend',
                    component:StockTrend
                },
                {
                    path: 'nodes',
                    name: 'Nodes',
                    component: Nodes
                },
                {
                    path: 'users',
                    name: 'Users',
                    component: Users
                },
                {
                    path: 'skumetas',
                    name: 'Skumeta',
                    component: Skumeta
                },
                {
                    path: 'department',
                    name: 'Department',
                    component: Departments
                },
                {
                    path: 'location',
                    name: 'Location',
                    component: Location
                },
                {
                    path: 'skumaster',
                    name: 'skumaster',
                    component: Skumaster
                },
                {
                    path: 'nodesku',
                    name: 'nodesku',
                    component: Nodesku
                },
                {
                    path: 'nodemap',
                    name: 'nodemap',
                    component: Nodemap
                },
                {
                    path: 'nodemapchart',
                    name: 'Node Map Chart',
                    component:Nodemapchart
                },
                {
                    path: 'trendreport',
                    name: 'TrendReport',
                    component:TrendReport
                },
                {
                    path: 'suppliertrendreport',
                    name: 'SupplierTrendReport',
                    component:SupplierTrendReport
                },
                {
                    path:'nodeskutopperformer',
                    name: 'nodeskutopperformer',
                    component: NodeSkuTopPerformer
                },
                {
                    path:'dispatchreport',
                    name: 'dispatchreport',
                    component: DispatchReport
                },
                {
                    path:'/admin/stocktrendreport/:availability_zone',
                    name: 'stocktrendreport',
                    component: StockTrend
                },
                {
                    path:'/admin/nodestocknorm/:id',
                    name: ' Node Stock Norms',
                    component: NodeStockNorm
                },
                {
                    path:'/admin/nodeskutopperformer/',
                    name: 'nodeskutopformer',
                    component: NodeSkuTopPerformer
                },
                 {
                    path:'/admin/expandedbarchart/:barchartdata',
                    name: 'expandedbarchart',
                    component: ExpandedBarChart
                },
                {
                    path: 'userrole',
                    name: 'UserRole',
                    component: UserRole
                },
                {
                    path: 'skuattribute',
                    name: 'skuattribute',
                    component: SKUAttribute
                }

            ]
        },
        {
            path: '/user',
            redirect: '/login',
            component: TheContainer,
            children: [
                {
                    path: 'dashboard',
                    name: 'dashboard',
                    component: UserDashboard
                },
                {
                    path: 'stocknorm',
                    name: 'Stocknorm',
                    component: UserStocknorm
                },
                {
                    path: 'suppliertrend/:id',
                    name: 'Supplier Trend',
                    component: UserSupplierTrend
                },
                {
                    path: 'skutrend/:id',
                    name: 'SKU Trend',
                    component: UserSKUTrend
                },
                {
                    path: 'consumertrend/:id',
                    name: 'Consumer Trend',
                    component: UserConsumerTrend
                },
              ]
            }
    ]
}
