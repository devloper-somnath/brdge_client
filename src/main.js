import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import VueTippy, { TippyComponent } from "vue-tippy";

import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import SessionManager from './utils/sessionManager'
 // Date Picker depedency
import datePicker from 'vue-bootstrap-datetimepicker'
// import 'bootstrap/dist/css/bootstrap.css'
//import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
//  call the component globally
Vue.component('datePicker', datePicker)
Vue.use(require('vue-moment'));
Vue.use(VueTippy);
Vue.component("tippy", TippyComponent);

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.use(new SessionManager())

import "tippy.js/themes/light.css";


new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  }
})
