const USER_ROLES = {
    ADMIN: "admin",
    USER: "user"
}

const AVAILABILITY_ZONE = {
    0 :"BLACK",
    1 :"RED",
    2 :"YELLOW",
    3 :"GREEN"
    4 : "Blue"


}


export { USER_ROLES, AVAILABILITY_ZONE}