import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.SKUMASTER, data)
}

function fetchList() {
   
    return APIBuilder.buildGetRequest(URLConstants.SKUMASTERS)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.SKUMASTER + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.SKUMASTER + '/' + id, data)
}

function deleteSkumaster(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.SKUMASTER + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteSkumaster
}