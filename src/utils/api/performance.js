import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'


function fetchListForNode() {
   
    return APIBuilder.buildGetRequest(URLConstants.BESTNODE)
}

function fetchListForSKU() {
   
    return APIBuilder.buildGetRequest(URLConstants.BESTSKU)
}




export {
   
    fetchListForNode,
    fetchListForSKU
    
}