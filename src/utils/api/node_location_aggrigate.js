import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

// function add(data) {
//     return APIBuilder.buildPostRequest(URLConstants.NODELOCATIONAGGRIGATE, data)
// }


// fetch  daily data for line chart

function fetchDayWiseData(parentid) {
    //console.log("parentid in util dailynodelocationaggrigate",parentid)
    if(parentid==null) {
      return APIBuilder.buildGetRequest(URLConstants.DAILYNODELOCATIONAGGRIGATES)
    }
    else {
      return APIBuilder.buildGetRequest(URLConstants.DAILYNODELOCATIONAGGRIGATES+"?parentid=" + parentid)
    }
}


// fetch  weekly data for line chart

function fetchWeekWiseData(parentid) {
    //console.log("parentid in util weeklynodelocationaggrigate",parentid)
  if(parentid==null){
    return APIBuilder.buildGetRequest(URLConstants.WEEKLYNODELOCATIONAGGRIGATES)
  }
  else {
    return APIBuilder.buildGetRequest(URLConstants.WEEKLYNODELOCATIONAGGRIGATES+"?parentid="+parentid)
  }
}


// fetch  monthly data for line chart

function fetchMonthWiseData(parentid) {
    //console.log("parentid in util monthlynodelocationaggrigate",parentid)
    if(parentid==null){
    return APIBuilder.buildGetRequest(URLConstants.MONTHLYNODELOCATIONAGGRIGATES)
   }
    else {
    return APIBuilder.buildGetRequest(URLConstants.MONTHLYNODELOCATIONAGGRIGATES+"?parentid="+parentid)
 }
   
}


export {
   
    fetchDayWiseData,
    fetchWeekWiseData,
    fetchMonthWiseData,
    
}