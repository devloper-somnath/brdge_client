import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.STOCKNORM, data)
}

function fetchList(availability_zone) {
    if(availability_zone!=null) {
       return APIBuilder.buildGetRequest(URLConstants.STOCKNORMS+"?availability_zone="+availability_zone) 
    }
    else {
        return APIBuilder.buildGetRequest(URLConstants.STOCKNORMS) 
    }
   
    
}

function nodeStockNorm(nodeid) {
    console.log("node id in util for nodestocknormis",nodeid)
    return APIBuilder.buildGetRequest(URLConstants.NODESTOCKNORMS+"?nodeid="+nodeid) 
    
    }
//accessing summary of stock nomrs

function summaryList() {
     return APIBuilder.buildGetRequest(URLConstants.STOCK_NORMS)
} 


//fetch NodeWise data where  city location id found

// function fetchNodeWiseData(cityid) {
//      return APIBuilder.buildGetRequest(URLConstants.STOCK_NORMS+"?cityid="+cityid)
// } 

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.STOCKNORM + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.STOCKNORM + '/' + id, data)
}

function deleteStockNorm(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.STOCKNORM + '/' + id)
}


export {
    add,
    fetchList,
    summaryList,
    nodeStockNorm,
    details,
    update,
    deleteStockNorm
}