import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    //console.log("node data is ",data)
    return APIBuilder.buildPostRequest(URLConstants.NODE, data)
}

function fetchList() {
    
    return APIBuilder.buildGetRequest(URLConstants.NODES)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.NODE + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.NODE + '/' + id, data)
}

function deleteNode(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.NODE + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteNode
}