import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.DEPARTMENT, data)
}

function fetchList() {
   
    return APIBuilder.buildGetRequest(URLConstants.DEPARTMENTS)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.DEPARTMENT + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.DEPARTMENT + '/' + id, data)
}

function deleteDepartment(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.DEPARTMENT + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteDepartment
}