import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.SKUMETA, data)
}

function fetchList() {
    
    return APIBuilder.buildGetRequest(URLConstants.SKUMETAS)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.SKUMETA + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.SKUMETA + '/' + id, data)
}

function deleteSkumeta(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.SKUMETA + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteSkumeta
    
}