import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

// add data to nodesku table through file

function addThroughFile (data) {
  console.log("data is ",data)
 let fd = new FormData();
    if (data.file) {
        fd.append('files', data.file);
        console.log('files in util', data.file)
   }
     return APIBuilder.buildPostRequest(URLConstants.NODESKUTHROUGHFILE, fd)
}

function add(data) {
  console.log("api data is",data)
    return APIBuilder.buildPostRequest(URLConstants.NODESKU, data)
}

// retrive data by availabiltiy zone to show dashbord tabel
function fetchList(availability_zone) {

     if(availability_zone!=null) {
      return APIBuilder.buildGetRequest(URLConstants.NODESKUBYZONE+"?availability_zone=" +availability_zone)
     }
     else {
        return APIBuilder.buildGetRequest(URLConstants.NODESKUBYZONE)
     }
   
    
}

// retrive data by paretid to show table conetent after click on bar chart

function NodefetchList(paretid) {
  console.log("utils parentid is",parentid)
    return APIBuilder.buildGetRequest(URLConstants.NODESKUBYPARENT+"?parentid=" +parentid)
}

// get sku by node

// function fetchSKUByNodeList(node) {
//   //console.log("network call nodeid",node)
//     if(node!=null) {
//       return APIBuilder.buildGetRequest(URLConstants.NODESKUS  +"?node="+node)  
//     }
//     else {
//         return APIBuilder.buildGetRequest(URLConstants.NODESKUS )  
//     }


//     //console.log(node)
    
// }


function fetchSKUByNodeList(id) {
  console.log("network call nodeid",id)
    if(id!=null) {
      return APIBuilder.buildGetRequest(URLConstants.NODESKUS  +"?id="+id)  
    }
    else {
        return APIBuilder.buildGetRequest(URLConstants.NODESKUS )  
    }


    //console.log(node)
    
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.NODESKU + "/" + id)
}

function searchNodeSKUName(name) {
   console.log("name in urlConstants is ",name)
    return APIBuilder.buildGetRequest(URLConstants.SERACHNODESKUNAME +"/"+ name)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.NODESKU + '/' + id, data)
}

function deleteNodesku(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.NODESKU + '/' + id)
}


export {
    addThroughFile,
    add,
    fetchList,
    details,
    update,
    deleteNodesku,
    fetchSKUByNodeList,
    NodefetchList,
    searchNodeSKUName
}