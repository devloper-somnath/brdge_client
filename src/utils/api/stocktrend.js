import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.STOCKTREND, data)
}

function fetchList(skuid) {
  
    if (skuid==null){
        return APIBuilder.buildGetRequest(URLConstants.STOCKTRENDS)
    }
    return APIBuilder.buildGetRequest(URLConstants.STOCKTRENDS+"?skuid="+skuid)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.STOCKTREND + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.STOCKTREND + '/' + id, data)
}

function deleteDailysns(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.STOCKTREND + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteDailysns
}