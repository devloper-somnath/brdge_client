import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    
    console.log(URLConstants.USERS)
    return APIBuilder.buildPostRequest(URLConstants.USERS, data)
}

function fetchList() {
    
    return APIBuilder.buildGetRequest(URLConstants.USERS)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.USER + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.USER + '/' + id, data)
}

function deleteUser(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.USER + '/' + id)
}

function getDashboardData(parentId, level, date) {
    let query = '?';
    if(parentId) {
            query += 'parent_id=' + parentId + '&'
    }
    query += 'level=' + level + '&' + 'date=' + date

    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/dashboard' + query);
}

function getStocknorm(parentId, date, type) {
    let query = '?';
    if(parentId) {
            query += 'parent_id=' + parentId + '&'
    }

    if(type) {
        query += 'type=' + type
    }
    // query += 'level=' + level + '&' + 'date=' + date

    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/stocknorm' + query);
}

function getLocations(level, parentId) {
    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/locations?level=' + level + '&parent_id=' + parentId);
}

function getNodesBySkuChangeStatus(availabilityZone) {
    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/nodesbychange?availability_zone=' + availabilityZone);
}

function search(query) {
    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/search/' + query);
}

function getSupplierTrend(id) {
    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/suppliertrend/' + id);
};

function getSkuTrend(id) {
    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/skutrend/' + id);
};

function getConsumerTrend(id) {
    return APIBuilder.buildGetRequest(URLConstants.USER + '/me/consumertrend/' + id);
};

export {
    add,
    fetchList,
    details,
    update,
    deleteUser,
    getDashboardData,
    getStocknorm,
    getLocations,
    getNodesBySkuChangeStatus,
    search,
    getSupplierTrend,
    getSkuTrend,
    getConsumerTrend
}