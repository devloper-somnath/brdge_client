import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data,id) {
    // console.log("node sku id in util ",data)
    return APIBuilder.buildPostRequest(URLConstants.NODESKUCOMMENT + '/' + data, id)
}

function addByIds(node, sku, data) {
    return APIBuilder.buildPostRequest(URLConstants.NODESKUCOMMENT + '/' + node + '/' + sku, data)
}

function fetchList(id) {
   console.log("util id for fetchList",id)
    return APIBuilder.buildGetRequest(URLConstants.NODESKUCOMMENT + "/" + id)
}

function fetchListByIds(node, sku) {
     return APIBuilder.buildGetRequest(URLConstants.NODESKUCOMMENT + "/" + node + '/' + sku)
 }

function deleteNodeSkuComment(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.NODESKUCOMMENT + '/' + id)
}


export {
    add,
    addByIds,
    fetchList,
    fetchListByIds,
    deleteNodeSkuComment
}