import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.NODEMAP, data)
}

function fetchList(supplier,child) {
    // console.log(supplier);
    let url = URLConstants.NODEMAPS;
        let restUrl = {};
        if (supplier!=null && child!=null) {
                let fullUrl = "?supplier= && child=";
                restUrl[fullUrl + supplier + child];
        }
        if (supplier!=null) {
            let supplierUrl = "?supplier = supplier";
            restUrl[supplierUrl + supplier]
        }
         if (child!=null) {
            let childUrl = "?child=" ;
            restUrl[supplierUrl + child]
        }
            
    return APIBuilder.buildGetRequest(url,restUrl)
    }
        
        

function fetchSupplierList(child) {
    return APIBuilder.buildGetRequest(URLConstants.NODEMAPS  +"?child="+child)
}
function fetchConsumerList(supplier) {
   return APIBuilder.buildGetRequest(URLConstants.NODEMAPS  +"?supplier="+supplier)
}


function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.NODEMAP + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.NODEMAP + '/' + id, data)
}

function deleteNodemap(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.NODEMAP + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteNodemap,
    fetchSupplierList,
    fetchConsumerList
}