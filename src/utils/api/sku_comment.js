import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data,id) {
    // console.log("node sku id in util ",data)
    return APIBuilder.buildPostRequest(URLConstants.SKUCOMMENT + '/' + data, id)
}

function fetchList(id) {
   console.log("util id for fetchList",id)
    return APIBuilder.buildGetRequest(URLConstants.SKUCOMMENT + "/" + id)
}



function deleteSkuComment(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.SKUCOMMENT + '/' + id)
}


export {
    add,
    fetchList,
    deleteSkuComment
}