import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'


function login (username, password) {
  return APIBuilder.buildPostRequest(URLConstants.LOGIN, { email: username, password: password })
}

function genericLogin (username, password) {
  const data = { email: username, password: password}
  return APIBuilder.buildPostRequest(URLConstants.LOGIN + 'generic', data)
}

export { login, genericLogin }
