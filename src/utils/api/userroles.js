import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    console.log("location data is",data)
    return APIBuilder.buildPostRequest(URLConstants.USERROLE, data)
}

function fetchList() {
   return APIBuilder.buildGetRequest(URLConstants.USERROLES)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.USERROLE + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.USERROLE + '/' + id, data)
}

function deleteUserRole(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.USERROLE + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteUserRole
}