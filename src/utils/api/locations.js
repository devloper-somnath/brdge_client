import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    console.log("location data is",data)
    return APIBuilder.buildPostRequest(URLConstants.LOCATION, data)
}

// allLocationList for fetching all records from database
function allLocationList () {
    return APIBuilder.buildGetRequest(URLConstants.ALLLOCATIONS)
}


// fetchList for line chart data

function fetchList(parentid) {
     console.log("parentid in util",parentid)
     
      if (parentid==null) {
        
          return APIBuilder.buildGetRequest(URLConstants.LOCATIONS)
        }
        else {
            return APIBuilder.buildGetRequest(URLConstants.LOCATIONS+"?parentid=" + parentid)
        }
    
}




function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.LOCATION + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.LOCATION + '/' + id, data)
}

function deleteLocation(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.LOCATION + '/' + id)
}


export {
    add,
    fetchList,
    allLocationList,
    details,
    update,
    deleteLocation
}