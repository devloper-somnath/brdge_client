import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    return APIBuilder.buildPostRequest(URLConstants.SKUAVAILABILITY, data)
}

// function fetchList(parentid) {
//     //console.log("parentid in util is",parentid)
//     if(parentid==null){
//         return APIBuilder.buildGetRequest(URLConstants.SKUAVAILABILITYS )
//     }
//     else {
//         return APIBuilder.buildGetRequest(URLConstants.SKUAVAILABILITYS +"?parentid="+parentid)
//     }
                                                                                    
// }

 // data retrive for second table on dashboard on first tab click
function fetchListByColor(color) {
    
 return APIBuilder.buildGetRequest(URLConstants.SKUAVAILABILITYS +"?color="+color)
}



// // data retrive for second table on dashboard on first tab click

function fetchListByDaysInColor(daysincolor) {
   
 return APIBuilder.buildGetRequest(URLConstants.SKUAVAILABILITY +"?daysincolor="+daysincolor)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.SKUAVAILABILITY + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.SKUAVAILABILITY + '/' + id, data)
}

function deleteskuavailability(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.SKUAVAILABILITY + '/' + id)
}


export {
    add,
    fetchListByColor,
    fetchListByDaysInColor,
    details,
    update,
    deleteskuavailability
}