import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
   // console.log("laundry category")
    console.log(URLConstants.LAUNDRY_CATEGORY)
    return APIBuilder.buildPostRequest(URLConstants.LAUNDRY_CATEGORY, data)
}

function fetchList() {
   // console.log("laundry category")
    return APIBuilder.buildGetRequest(URLConstants.LAUNDRY_CATEGORY)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.FIRM + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.FIRM + '/' + id, data)
}

function deleteFirm(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.FIRM + '/' + id)
}

function addUser(id, email) {
    const data = { user: email, firm: id }
    return APIBuilder.buildPutRequest(URLConstants.FIRM_ADD_USER, data)
}

function getUsers(id) {
    return APIBuilder.buildGetRequest(URLConstants.USERS + "?firm=" + id);
}

function deleteUser(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.USER + "/" + id);
}

export {
    add,
    fetchList,
    details,
    update,
    deleteFirm,
    addUser,
    getUsers,
    deleteUser
}