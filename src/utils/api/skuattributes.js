import * as APIBuilder from './apiBuilder'
import * as URLConstants from '../urlConstants'

function add(data) {
    console.log("sku attribute is",data)
    return APIBuilder.buildPostRequest(URLConstants.SKUATTRIBUTE, data)
}

function fetchList() {
   return APIBuilder.buildGetRequest(URLConstants.SKUATTRIBUTES)
}

function details(id) {
    return APIBuilder.buildGetRequest(URLConstants.SKUATTRIBUTE + "/" + id)
}

function update(id, data) {
    return APIBuilder.buildPutRequest(URLConstants.SKUATTRIBUTE + '/' + id, data)
}

function deleteSkuAttribute(id) {
    return APIBuilder.buildDeleteRequest(URLConstants.SKUATTRIBUTE + '/' + id)
}


export {
    add,
    fetchList,
    details,
    update,
    deleteSkuAttribute
}