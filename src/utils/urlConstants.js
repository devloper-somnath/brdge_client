// const BASE_URL = 'http://localhost:3000'
// const BASE_URL = 'http://192.168.42.219:3000'
//const BASE_URL = 'http://brdge.herokuapp.com'

 const BASE_URL = 'http://localhost:3000'
//  const BASE_URL = 'http://3.131.180.31:3000'


const LOGIN = BASE_URL + '/api/auth/signin'

const STAFF_RESET_PASSWORD = BASE_URL + '/api/user/resetpassword/'

// To create Node
const NODE =BASE_URL + '/api/node'
const NODES= BASE_URL + '/api/nodes'

// To create SkuMetas
const SKUMETAS =BASE_URL + '/api/skumetas'
const SKUMETA = BASE_URL + '/api/skumeta'

// to create department
const DEPARTMENTS = BASE_URL + '/api/departments'
const DEPARTMENT = BASE_URL + '/api/department'

//to create locations
const ALLLOCATIONS =BASE_URL +'/api/alllocations'
const LOCATIONS = BASE_URL + '/api/locations'
const LOCATION = BASE_URL + '/api/location'
//const PARENTLOCATION =BASE_URL + '/api/parentlocation'

// to create skumasters
const SKUMASTERS =BASE_URL + '/api/skumasters'
const SKUMASTER =BASE_URL + '/api/skumaster'

// to create Node SKUs
const NODESKUS =BASE_URL + '/api/nodeskus'
const NODESKU =BASE_URL + '/api/nodesku'
const NODESKUBYZONE =BASE_URL +'/api/nodeskubyzone'
const NODESKUBYPARENT =BASE_URL + '/api/nodeskubyparentid'
const NODESKUTHROUGHFILE = BASE_URL +'/api/nodeskufromfile'
const SERACHNODESKUNAME = BASE_URL + '/api/nodeskus/search'

//to create Node Maps
const NODEMAPS =BASE_URL + '/api/nodemaps'
const NODEMAP=BASE_URL + '/api/nodemap'

// to create Stock Norms
const STOCKNORMS =BASE_URL + '/api/stocknorms'
const STOCKNORM=BASE_URL + '/api/stocknorm'
const STOCK_NORMS = BASE_URL +'/api/stocknorm/summary'
const STOCK_NORM= BASE_URL + '/api/stocknorm/location'
const NODESTOCKNORMS = BASE_URL + '/api/nodestocknorm'

// daily SNS data (stock trend)
const STOCKTRENDS = BASE_URL +'/api/stocktrenddata'
const STOCKTREND = BASE_URL +'/api/stocktrend'


//Node Location Aggrigate (for line chart)
//const NODELOCATIONAGGRIGATE = BASE_URL + '/api/dailynodelocationparentdata'

// DAILY nodelocation aggrigate data for line chart
const DAILYNODELOCATIONAGGRIGATES = BASE_URL + '/api/dailynodelocationparentdata'

// WEEKLY nodelocation aggrigate data for line chart
const WEEKLYNODELOCATIONAGGRIGATES = BASE_URL + '/api/weeklynodelocationparentdata'

// MONTHLY nodelocation aggrigate data for line chart
const MONTHLYNODELOCATIONAGGRIGATES = BASE_URL + '/api/monthlynodelocationparentdata'


// Node Sku comment
const NODESKUCOMMENT = BASE_URL + '/api/comments/nodesku'

// SKU Comment
const SKUCOMMENT = BASE_URL + '/api/comments/sku'


// sku avalabilites
 const SKUAVAILABILITY = BASE_URL + '/api/getdaysincolorcount'
 const SKUAVAILABILITYS = BASE_URL + '/api/getcolorcounter'

// best performing node

const BESTNODE = BASE_URL +'/api/performance/node'
const BESTSKU  = BASE_URL +'/api/performance/sku'


//user role
const USERROLE = BASE_URL + '/api/userrole'
const USERROLES = BASE_URL + '/api/alluserrole'

// sku attribute
const SKUATTRIBUTE = BASE_URL + '/api/skuattribute'
const SKUATTRIBUTES = BASE_URL + '/api/allskuattributes'




// To create firm admins
const ADMIN = BASE_URL + '/api/admin'

// To create users
const USERS = BASE_URL + '/api/users'
const USER = BASE_URL + '/api/user'


export {
    BASE_URL,
    LOGIN,
    STAFF_RESET_PASSWORD,

    USERS,
    USER,
    NODE,
    NODES,
    SKUMETA,
    SKUMETAS,
    DEPARTMENT,
    DEPARTMENTS,
    LOCATION,
    LOCATIONS,
    ALLLOCATIONS,
    //PARENTLOCATION,
    SKUMASTER,
    SKUMASTERS,
    NODESKU,
    NODESKUS,
    NODESKUBYZONE,
    NODESKUBYPARENT,
    NODESKUTHROUGHFILE,
    NODEMAP,
    NODEMAPS,
    STOCKNORM,
    STOCKNORMS,
    STOCK_NORMS,
    STOCK_NORM,
    STOCKTRENDS,
    NODESTOCKNORMS,
    STOCKTREND,
    DAILYNODELOCATIONAGGRIGATES,
    WEEKLYNODELOCATIONAGGRIGATES,
    MONTHLYNODELOCATIONAGGRIGATES,
    NODESKUCOMMENT,
    SKUAVAILABILITY,
    SKUAVAILABILITYS,
    SKUCOMMENT,
    BESTNODE,
    BESTSKU,
    SERACHNODESKUNAME,
    USERROLE,
    USERROLES,
    SKUATTRIBUTE,
    SKUATTRIBUTES


}
