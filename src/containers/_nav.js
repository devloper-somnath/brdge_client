export default {
  "super_admin": [{
      _name: 'CSidebarNav',
      _children: [{
              _name: 'CSidebarNavItem',
              name: 'Dashboard',
              to: '/admin/dashboard',
              icon: 'cil-center-focus',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'UserRole',
              to: '/admin/userrole',
              icon: 'cil-speedometer',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'Admins',
              to: '/admin/users',
              icon: 'cil-user',
          },
          {
          _name: 'CSidebarNavDropdown',
          name: 'Report',
          //route: '/base',
          icon: 'cil-puzzle',
          items: [

           {
              _name: 'CSidebarNavItem',
              name: 'Stock Norms',
              to: '/admin/stocknorms',
              icon: 'cil-speedometer',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'TrendReport',
              to: '/admin/TrendReport',
              icon: 'cil-speedometer',
          },
          // {
          //     _name: 'CSidebarNavItem',
          //     name: 'SupplierTrendReport',
          //     to: '/admin/SupplierTrendReport',
          //     icon: 'cil-speedometer',
          // },
           {
              _name: 'CSidebarNavItem',
              name: 'Node SKU',
              to: '/admin/nodesku',
              icon: 'cil-speedometer',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'Stock Trend',
              to: '/admin/stocktrendreport',
              icon: 'cil-chart-line',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'SKUAttribute',
              to: '/admin/skuattribute',
              icon: 'cil-speedometer',
          },

          ],
      },



          {
          _name: 'CSidebarNavDropdown',
          name: 'Setup',
          //route: '/base',
          icon: 'cil-puzzle',
          items: [


          {
              _name: 'CSidebarNavItem',
              name: 'Nodes',
              to: '/admin/nodes',
              icon: 'cil-sitemap',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'SKU Meta',
              to: '/admin/skumetas',
              icon: 'cil-cart',
          },

          {
              _name: 'CSidebarNavItem',
              name: 'Department',
              to: '/admin/department',
              icon: 'cil-speedometer',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'Location',
              to: '/admin/location',
              icon: 'cil-speedometer',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'SKU Master',
              to: '/admin/skumaster',
              icon: 'cil-cart',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'Node Map',
              to: '/admin/nodemap',
              icon: 'cil-speedometer',
          },
          {
              _name: 'CSidebarNavItem',
              name: 'Node Map Chart',
              to: '/admin/nodemapchart',
              icon: 'cil-speedometer',
          },
           {
              _name: 'CSidebarNavItem',
              name: 'dispatchreport',
              to: '/admin/dispatchreport',
              icon: 'cil-arrow-thick-from-right',
          },
          //  {
          //     _name: 'CSidebarNavItem',
          //     name: 'NodeSkutopPerformer',
          //     to: '/admin/nodeskutopperformer',
          //     icon: 'cil-speedometer',
          // }
            ]
        },









      ]
  }],
  "user" : [
    {
      _name: 'CSidebarNavItem',
      name: 'Dashboard',
      to: '/user/dashboard',
      icon: 'cil-center-focus',
    },
    {
        _name: 'CSidebarNavItem',
        name: 'Stock Norms',
        to: '/user/stocknorms',
        icon: 'cil-speedometer',
    },
  ]
}
